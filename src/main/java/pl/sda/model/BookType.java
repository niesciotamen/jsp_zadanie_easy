package pl.sda.model;

public enum BookType {
    ADVENTURE,
    ACTION,
    SCIENCE;
}
