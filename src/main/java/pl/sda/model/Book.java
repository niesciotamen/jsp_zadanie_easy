package pl.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Book implements IBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String publishedDate;

    @Enumerated(EnumType.STRING)
    private BookType bookType;
    private int numberOfPages;

    @ManyToOne
    private Publisher publisher;

    @ManyToOne
    private Author author;

}
