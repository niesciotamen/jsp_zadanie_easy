package pl.sda.servlet;

import pl.sda.database.EntityDao;
import pl.sda.model.Author;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/author/add")
public class AuthorFormServlet extends HttpServlet {
    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");

        req.setAttribute("id", id);
        req.getRequestDispatcher("/author/form.jsp").forward(req, resp);
    }

    // post - wyślij treść formularza na serwer
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Author author = new Author();
        author.setName(req.getParameter("name"));
        author.setSurname(req.getParameter("surname"));
        author.setYearOfBirth(req.getParameter("yearOfBirth"));
        author.setPlaceOfBirth(req.getParameter("placeOfBirth"));

        entityDao.saveOrUpdate(author);

        resp.sendRedirect("/author/list");
    }
}
