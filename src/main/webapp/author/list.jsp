<%@ page import="pl.sda.model.Author" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: nesciotamen
  Date: 2019-07-26
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Author List</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>
<table>
    <tr>
        <th style="width: 150px;">Name</th>
        <th style="width: 150px;">Surname</th>
        <th style="width: 150px;">Year of Borth</th>
        <th style="width: 150px;">Place of Borth</th>
        <th style="width: 150px;">Books</th>
        <th style="width: 150px;">Add Book</th>
    </tr>
    <%--Nagłówek--%>
    <%
        List<Author> authors = (List<Author>) request.getAttribute("authorList");
        for (int i = 0; i < authors.size(); i++) {
            Author author = authors.get(i);

            out.print("<tr>");
            out.print("<td>" + author.getName() + "</td>");
            out.print("<td>" + author.getSurname() + "</td>");
            out.print("<td>" + author.getYearOfBirth() + "</td>");
            out.print("<td>" + author.getPlaceOfBirth() + "</td>");
            out.print("<td>" +
                    "<a href=\"/book/list?authorId=" + author.getId() + "\">Books</a>" +
                    "</td>");
            out.print("<td>" +
                    "<a href=\"/book/add?authorId=" + author.getId() + "\">Add book</a>" +
                    "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
